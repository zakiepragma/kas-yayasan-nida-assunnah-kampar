import { Fragment } from 'react'
import { Disclosure, Menu, Transition } from '@headlessui/react'
import { Bars3Icon, BellIcon, XMarkIcon } from '@heroicons/react/24/outline'

const user = {
  name: 'El-Owais',
  email: 'elowais@attarmiz.tech',
  imageUrl: '/ikhwan.jpeg',
}
const navigation = [
  { name: "Yayasan Nida' As-Sunnah Kampar", href: '#', current: true },
]
const userNavigation = [
  { name: 'Your Profile', href: '#' },
  { name: 'Settings', href: '#' },
  { name: 'Sign out', href: '#' },
]

function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

function format_uang(num) {
  return 'Rp. ' + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

const kas_november_2022 = [
  {
    uraian: 'ambil uang dari brangkas sekolah',
    tanggal: '1 november 2022',
    uang_masuk: 15000000,
    uang_keluar: 0,
    keterangan: 'diketahui zakie dan arfit',
  },
  {
    uraian: 'ambil uang dari brangkas sekolah',
    tanggal: '1 november 2022',
    uang_masuk: 15000000,
    uang_keluar: 0,
    keterangan: 'diketahui zakie dan arfit',
  },
  {
    uraian: 'ambil uang dari brangkas sekolah',
    tanggal: '1 november 2022',
    uang_masuk: 15000000,
    uang_keluar: 0,
    keterangan: 'diketahui zakie dan arfit',
  },
  {
    uraian: 'ambil uang dari brangkas sekolah',
    tanggal: '1 november 2022',
    uang_masuk: 15000000,
    uang_keluar: 0,
    keterangan: 'diketahui zakie dan arfit',
  },
  {
    uraian: 'ambil uang dari brangkas sekolah',
    tanggal: '1 november 2022',
    uang_masuk: 15000000,
    uang_keluar: 0,
    keterangan: 'diketahui zakie dan arfit',
  },
  {
    uraian: 'ambil uang dari brangkas sekolah',
    tanggal: '1 november 2022',
    uang_masuk: 15000000,
    uang_keluar: 0,
    keterangan: 'diketahui zakie dan arfit',
  },
  {
    uraian: 'ambil uang dari brangkas sekolah',
    tanggal: '1 november 2022',
    uang_masuk: 15000000,
    uang_keluar: 0,
    keterangan: 'diketahui zakie dan arfit',
  },
  {
    uraian: 'ambil uang dari brangkas sekolah',
    tanggal: '1 november 2022',
    uang_masuk: 15000000,
    uang_keluar: 0,
    keterangan: 'diketahui zakie dan arfit',
  },
  {
    uraian: 'ambil uang dari brangkas sekolah',
    tanggal: '1 november 2022',
    uang_masuk: 15000000,
    uang_keluar: 0,
    keterangan: 'diketahui zakie dan arfit',
  },
  {
    uraian: 'ambil uang dari brangkas sekolah',
    tanggal: '1 november 2022',
    uang_masuk: 15000000,
    uang_keluar: 0,
    keterangan: 'diketahui zakie dan arfit',
  },
  {
    uraian: 'ambil uang dari brangkas sekolah',
    tanggal: '1 november 2022',
    uang_masuk: 15000000,
    uang_keluar: 0,
    keterangan: 'diketahui zakie dan arfit',
  },
  {
    uraian: 'ambil uang dari brangkas sekolah',
    tanggal: '1 november 2022',
    uang_masuk: 15000000,
    uang_keluar: 0,
    keterangan: 'diketahui zakie dan arfit',
  },
]

export default function App() {
  return (
    <>
      {/*
        This example requires updating your template:

        ```
        <html className="h-full bg-gray-100">
        <body className="h-full">
        ```
      */}
      <div className='min-h-full'>
        <Disclosure as='nav' className='bg-gray-800'>
          {({ open }) => (
            <>
              <div className='mx-auto max-w-7xl px-4 sm:px-6 lg:px-8'>
                <div className='flex h-16 items-center justify-between'>
                  <div className='flex items-center'>
                    <div className='flex-shrink-0'>
                      <img
                        className='h-8 w-8'
                        src='/logo-yna.png'
                        alt='Your Company'
                      />
                    </div>
                    <div className='hidden md:block'>
                      <div className='ml-10 flex items-baseline space-x-4'>
                        {navigation.map((item) => (
                          <a
                            key={item.name}
                            href={item.href}
                            className={classNames(
                              item.current
                                ? 'bg-gray-800 text-white text-xl'
                                : 'text-gray-300 hover:bg-gray-700 hover:text-white text-xl',
                              'px-3 py-2 rounded-md text-sm font-medium'
                            )}
                            aria-current={item.current ? 'page' : undefined}
                          >
                            {item.name}
                          </a>
                        ))}
                      </div>
                    </div>
                  </div>
                  <div className='hidden md:block'>
                    <div className='ml-4 flex items-center md:ml-6'>
                      <button
                        type='button'
                        className='rounded-full bg-gray-800 p-1 text-gray-400 hover:text-white focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-gray-800'
                      >
                        <span className='sr-only'>View notifications</span>
                        <BellIcon className='h-6 w-6' aria-hidden='true' />
                      </button>

                      {/* Profile dropdown */}
                      <Menu as='div' className='relative ml-3'>
                        <div>
                          <Menu.Button className='flex max-w-xs items-center rounded-full bg-gray-800 text-sm focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-gray-800'>
                            <span className='sr-only'>Open user menu</span>
                            <img
                              className='h-8 w-8 rounded-full'
                              src={user.imageUrl}
                              alt=''
                            />
                          </Menu.Button>
                        </div>
                        <Transition
                          as={Fragment}
                          enter='transition ease-out duration-100'
                          enterFrom='transform opacity-0 scale-95'
                          enterTo='transform opacity-100 scale-100'
                          leave='transition ease-in duration-75'
                          leaveFrom='transform opacity-100 scale-100'
                          leaveTo='transform opacity-0 scale-95'
                        >
                          <Menu.Items className='absolute right-0 z-10 mt-2 w-48 origin-top-right rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none'>
                            {userNavigation.map((item) => (
                              <Menu.Item key={item.name}>
                                {({ active }) => (
                                  <a
                                    href={item.href}
                                    className={classNames(
                                      active ? 'bg-gray-100' : '',
                                      'block px-4 py-2 text-sm text-gray-700'
                                    )}
                                  >
                                    {item.name}
                                  </a>
                                )}
                              </Menu.Item>
                            ))}
                          </Menu.Items>
                        </Transition>
                      </Menu>
                    </div>
                  </div>
                  <div className='-mr-2 flex md:hidden'>
                    {/* Mobile menu button */}
                    <Disclosure.Button className='inline-flex items-center justify-center rounded-md bg-gray-800 p-2 text-gray-400 hover:bg-gray-700 hover:text-white focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-gray-800'>
                      <span className='sr-only'>Open main menu</span>
                      {open ? (
                        <XMarkIcon
                          className='block h-6 w-6'
                          aria-hidden='true'
                        />
                      ) : (
                        <Bars3Icon
                          className='block h-6 w-6'
                          aria-hidden='true'
                        />
                      )}
                    </Disclosure.Button>
                  </div>
                </div>
              </div>

              <Disclosure.Panel className='md:hidden'>
                <div className='space-y-1 px-2 pt-2 pb-3 sm:px-3'>
                  {navigation.map((item) => (
                    <Disclosure.Button
                      key={item.name}
                      as='a'
                      href={item.href}
                      className={classNames(
                        item.current
                          ? 'bg-gray-900 text-white'
                          : 'text-gray-300 hover:bg-gray-700 hover:text-white',
                        'block px-3 py-2 rounded-md text-base font-medium'
                      )}
                      aria-current={item.current ? 'page' : undefined}
                    >
                      {item.name}
                    </Disclosure.Button>
                  ))}
                </div>
                <div className='border-t border-gray-700 pt-4 pb-3'>
                  <div className='flex items-center px-5'>
                    <div className='flex-shrink-0'>
                      <img
                        className='h-10 w-10 rounded-full'
                        src={user.imageUrl}
                        alt=''
                      />
                    </div>
                    <div className='ml-3'>
                      <div className='text-base font-medium leading-none text-white'>
                        {user.name}
                      </div>
                      <div className='text-sm font-medium leading-none text-gray-400'>
                        {user.email}
                      </div>
                    </div>
                    <button
                      type='button'
                      className='ml-auto flex-shrink-0 rounded-full bg-gray-800 p-1 text-gray-400 hover:text-white focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-gray-800'
                    >
                      <span className='sr-only'>View notifications</span>
                      <BellIcon className='h-6 w-6' aria-hidden='true' />
                    </button>
                  </div>
                  <div className='mt-3 space-y-1 px-2'>
                    {userNavigation.map((item) => (
                      <Disclosure.Button
                        key={item.name}
                        as='a'
                        href={item.href}
                        className='block rounded-md px-3 py-2 text-base font-medium text-gray-400 hover:bg-gray-700 hover:text-white'
                      >
                        {item.name}
                      </Disclosure.Button>
                    ))}
                  </div>
                </div>
              </Disclosure.Panel>
            </>
          )}
        </Disclosure>

        <header className='bg-white shadow'>
          <div className='flex flex-row justify-end mx-auto max-w-7xl py-6 px-4 sm:px-6 lg:px-8'>
            <select
              id='country'
              name='country'
              autoComplete='country-name'
              className='basis-1/2 mt-1 block w-full mr-5 rounded-md border border-gray-300 bg-white py-2 px-3 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm'
            >
              <option>Februari 2023</option>
              <option>Januari 2023</option>
              <option>Desember 2022</option>
            </select>
            <button className='basis-1/6 border bg-red-500 text-white p-1 border-black-900 hover:border-cyan-400 hover:bg-green-100 hover:text-black'>
              <i className='fa fa-print'></i>
              Print PDF
            </button>
          </div>
        </header>
        <main>
          <div className='mx-auto max-w-7xl py-6 sm:px-6 lg:px-8'>
            {/* Replace with your content */}
            <div className='flex flex-col mb-20'>
              <div className='overflow-x-auto sm:mx-0.5 lg:mx-0.5'>
                <div className='py-2 inline-block min-w-full sm:px-6 lg:px-8'>
                  <div className='overflow-hidden'>
                    <table className='min-w-full'>
                      <thead className='bg-white border-b'>
                        <tr>
                          <th
                            scope='col'
                            className='text-sm font-medium text-gray-900 px-6 py-4 text-left'
                          >
                            #
                          </th>
                          <th
                            scope='col'
                            className='text-sm font-medium text-gray-900 px-6 py-4 text-left'
                          >
                            Uraian
                          </th>
                          <th
                            scope='col'
                            className='text-sm font-medium text-gray-900 px-6 py-4 text-left'
                          >
                            Tanggal
                          </th>
                          <th
                            scope='col'
                            className='text-sm font-medium text-gray-900 px-6 py-4 text-left'
                          >
                            Uang Masuk
                          </th>
                          <th
                            scope='col'
                            className='text-sm font-medium text-gray-900 px-6 py-4 text-left'
                          >
                            Uang Keluar
                          </th>
                          <th
                            scope='col'
                            className='text-sm font-medium text-gray-900 px-6 py-4 text-left'
                          >
                            Keterangan
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        {kas_november_2022.map((item, i) => (
                          <tr className='bg-gray-100 border-b'>
                            <td className='px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900'>
                              {i + 1}
                            </td>
                            <td className='text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap'>
                              {item.uraian}
                            </td>
                            <td className='text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap'>
                              {item.tanggal}
                            </td>
                            <td className='text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap'>
                              {format_uang(item.uang_masuk)}
                            </td>
                            <td className='text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap'>
                              {format_uang(item.uang_keluar)}
                            </td>
                            <td className='text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap'>
                              {item.keterangan}
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <footer className='fixed bottom-0 left-0 z-20 w-full p-4 bg-white border-t border-gray-200 shadow md:flex md:items-center md:justify-between md:p-6 dark:bg-gray-800 dark:border-gray-600'>
              <h6 className='text-sm hover:text-slate-900 text-slate-500 sm:text-center'>
                Total Uang Masuk : {format_uang(47000000)}
              </h6>
              <h6 className='text-sm hover:text-slate-900 text-slate-500 sm:text-center'>
                Total Uang Keluar : {format_uang(32000000)}
              </h6>
              <h6 className='text-sm hover:text-slate-900 text-slate-500 sm:text-center'>
                Total Sisa Kas : {format_uang(102000000)}
              </h6>
            </footer>
            {/* /End replace */}
          </div>
        </main>
      </div>
    </>
  )
}
